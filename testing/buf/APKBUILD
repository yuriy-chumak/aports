# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=buf
pkgver=1.15.1
pkgrel=0
pkgdesc="CLI to work with Protocol Buffers"
url="https://buf.build/"
# 32bit: fail tests with int overflow
arch="all !x86 !armhf !armv7"
license="Apache-2.0"
makedepends="go"
checkdepends="protobuf-dev"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	$pkgname-protoc-plugins:_protoc
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/bufbuild/buf/archive/refs/tags/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	for binary in buf protoc-gen-buf-breaking protoc-gen-buf-lint; do
		go build -v ./cmd/$binary
	done

	for shell in bash fish zsh; do
		./buf completion $shell > buf.$shell
	done
}

check() {
	# /private/pkg/git: try to clone a remote repo
	# /private/bufpkg/bufimage/bufimagebuild: TestCompareGoogleapis test fails
	# /private/buf/cmd/buf/command/{generate,alpha/protoc}: fails with a lot of unused proto import warnings
	go test $(go list ./... | grep -v \
		-e '/private/pkg/git$' \
		-e '/private/bufpkg/bufimage/bufimagebuild$' \
		-e '/private/buf/cmd/buf/command/generate$' \
		-e '/private/buf/cmd/buf/command/alpha/protoc$' \
	)
}

package() {
	install -Dm755 buf -t "$pkgdir"/usr/bin/

	install -Dm644 buf.bash \
		"$pkgdir"/usr/share/bash-completion/completions/buf
	install -Dm644 buf.fish \
		"$pkgdir"/usr/share/fish/completions/buf.fish
	install -Dm644 buf.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_buf

	install -Dm755 protoc-gen-buf-breaking protoc-gen-buf-lint \
		-t "$pkgdir"/usr/bin/
}

_protoc() {
	depends="protoc"

	amove usr/bin/protoc-gen-buf-*
}

sha512sums="
81f9d382544aa358866ed31acd63b860d0d99c3dc89f9ca329d7b3053a142eb49d87c1894db1de732f7ab8a4aebd49560922fc7b5090bddc5fc66741121058b5  buf-1.15.1.tar.gz
"
