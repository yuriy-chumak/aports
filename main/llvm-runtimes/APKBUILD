# Contributor: Eric Molitor <eric@molitor.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=llvm-runtimes
# Note: Update together with llvm.
pkgver=16.0.0
_llvmver=${pkgver%%.*}
pkgrel=3
pkgdesc="LLVM Runtimes"
url="https://llvm.org/"
arch="all"
license="Apache-2.0"
makedepends="
	clang
	cmake
	linux-headers
	llvm$_llvmver-dev
	llvm$_llvmver-static
	python3
	samurai
	"
subpackages="
	libc++:libcxx
	libc++-static:libcxx_static
	libc++-dev:libcxx_dev
	compiler-rt:rt
	llvm-libunwind:libunwind
	llvm-libunwind-static:libunwind_static
	llvm-libunwind-dev:libunwind_dev
	"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/llvm-project-${pkgver//_/}.src.tar.xz
	armv6-arch.patch.noauto
	compiler-rt-lsan-dtp-offset.patch
	compiler-rt-ppc-fixes.patch
	compiler-rt-sanitizer-supported-arch.patch
	compiler-rt-scudo-standalone.patch
	libunwind-link-libssp.patch
	"
builddir="$srcdir/llvm-project-${pkgver//_/}.src"
options="!check"

case "$CARCH" in
# Sanitizers are broken on other arches.
# Keep in sync with compiler-rt-sanitizer-supported-arch.patch.
aarch64|ppc64le|x86_64)
	_build_sanitizers='ON'
	;;
*)
	_build_sanitizers='OFF'
	;;
esac

case "$CARCH" in
# supported scudo architectures
aarch64|arm*|ppc64le|x86_64|x86)
	_build_scudo='ON'
	# be first to steal files from compiler-rt
	subpackages="scudo-malloc:scudo $subpackages"
	;;
*)
	_build_scudo='OFF'
	;;
esac

prepare() {
	default_prepare

	case "$CARCH" in
	armhf)
		patch -Np1 < "$srcdir"/armv6-arch.patch.noauto
		;;
	esac
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="
			-DCMAKE_SYSTEM_NAME=Linux
			-DCMAKE_HOST_SYSTEM_NAME=Linux
			-DLIBUNWIND_SYSROOT=$CBUILDROOT
			"
	fi

	CC=clang \
	CXX=clang++ \
	CFLAGS="$CFLAGS -DNDEBUG" \
	CXXFLAGS="$CXXFLAGS -DNDEBUG -DSCUDO_CAN_USE_PRIMARY64=0" \
	cmake -B build -G Ninja -Wno-dev -S runtimes \
		-DLLVM_ENABLE_RUNTIMES="compiler-rt;libunwind;libcxx;libcxxabi" \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SCUDO_STANDALONE="$_build_scudo" \
		-DLIBCXX_HAS_MUSL_LIBC=ON \
		-DLIBUNWIND_HAS_NODEFAULTLIBS_FLAG=OFF \
		-DCOMPILER_RT_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DCOMPILER_RT_BUILD_SANITIZERS=$_build_sanitizers \
		-DCOMPILER_RT_INSTALL_PATH="/usr/lib/llvm$_llvmver/lib/clang/$_llvmver" \
		-DCOMPILER_RT_BUILD_GWP_ASAN=OFF \
		-DCMAKE_VERBOSE_MAKEFILE=ON \
		$crossopts
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	mkdir -p "$pkgdir"/usr/include/mach-o
	cp libunwind/include/*.h "$pkgdir"/usr/include/
	cp libunwind/include/mach-o/*.h "$pkgdir"/usr/include/mach-o/

	case "$CARCH" in
	ppc64le)
		local archname="powerpc64le"
		;;
	arm*)
		local archname="armhf"
		;;
	x86)
		local archname="i386"
		;;
	*)
		local archname="$CARCH"
		;;
	esac

	if [ "$_build_scudo" = "ON" ]; then
		# provide an easier to find name for the scudo standalone allocator,
		# allowing preloading and patchelfind of something in /usr/lib instead of
		# a deep directory tree
		ln -sv /usr/lib/llvm$_llvmver/lib/clang/$_llvmver/lib/linux/libclang_rt.scudo_standalone-$archname.so \
			"$pkgdir"/usr/lib/libscudo.so
	fi
}

libunwind() {
	pkgdesc="LLVM libunwind library"
	depends="!libunwind-dev"

	amove usr/lib/libunwind.so.*
}

libunwind_static() {
	pkgdesc="LLVM libunwind library (static)"

	amove usr/lib/libunwind.a
}

libunwind_dev() {
	pkgdesc="LLVM libunwind library (development files)"

	amove usr/lib/libunwind.so
	amove usr/include
}

scudo() {
	pkgdesc="Standalone Scudo hardened allocator"

	amove usr/lib/libscudo.so
	# avoid moving _standalone_cxx
	amove usr/lib/llvm$_llvmver/lib/clang/$_llvmver/lib/linux/libclang_rt.scudo_standalone-*.so
}

rt() {
	pkgdesc="LLVM compiler-rt runtime libraries"

	amove usr/lib/llvm$_llvmver/lib/clang/$_llvmver

	if [ "$_build_scudo" = "ON" ]; then
		# it contains some rt files- the full compiler-rt set should also pull scudo
		depends="scudo-malloc=$pkgver-r$pkgrel"
	fi
}

libcxx() {
	pkgdesc="LLVM libc++ library"

	amove usr/lib/libc++*.so.*
}

libcxx_static() {
	pkgdesc="LLVM libc++ library (static libs)"

	amove usr/lib/libc++*.a
}

libcxx_dev() {
	pkgdesc="LLVM libc++ library (development files)"

	amove usr/lib/libc++*.so
	amove usr/include/c++
}

sha512sums="
3f040abc1b03205693824aeff2ee6efb0cff25fe04bd5265141c093f294655a1f3fcda73cab9c57cbed7523c8f186a7e2484afce0792c55e90e3fb80450fabb7  llvm-project-16.0.0.src.tar.xz
5e7bbddbaea902e5ba5cd4db78bedbeef216f44fdd9b8f73efde6c09f40115c078649a109ffa61fefa0ee2f26655c038a48589ecac83068a47d60e9248c5dff1  armv6-arch.patch.noauto
7c2cbd095b863f735842aaa8f0daecbf0282200fc58f1394139cee30d53c4a738757e38cbf0ec734398ee827e8a47314592bd7dc9768ef5c3664db682680e5a1  compiler-rt-lsan-dtp-offset.patch
c7b7deaedddc4b980b349c26751bdca102e19b3d2fb61fc47ede849d3994fd9b6bbcd631bc129f129f5b0a048e12ae04cce3a2d91bc5678aa9f782c37d170c93  compiler-rt-ppc-fixes.patch
448ad39865b1a86d69c80b6626500e16509834787944f0b402b3eb3b2d6d1c034073b4004b7e1eee6afdc21b553945e65943b79426105503012c4dfac1ee2e7b  compiler-rt-sanitizer-supported-arch.patch
6f38a5f033b855bcb1132fe219aac0fbf68cecb615734cc831ed356f7a30498bb014c0d39f306a182f0553a82232b6141318cd2b9a449e17e4488ea66811bd06  compiler-rt-scudo-standalone.patch
e601d0657d61cf080c9a03fd0139f9cae85339ba7f997f61283c1ba3b9fb48479cca875db44b64ee8b48a7d61a18a70d7cd123500f72966770cf5312b1aca034  libunwind-link-libssp.patch
"
