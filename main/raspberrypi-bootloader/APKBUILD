# Contributor: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=raspberrypi-bootloader
# To match Alpine kernel schedule, use master branch commit id rather than older stable tagged releases
# Keep by-the-date release numbering for consistency
_commit=fb2a9eb286caabd1e447f80de019a9167b473a64
pkgver=1.20230317
pkgrel=0
pkgdesc="Bootloader files for the Raspberry Pi"
url="https://github.com/raspberrypi/rpi-firmware"
arch="armhf armv7 aarch64"
license="custom"
options="!check !strip !tracedeps !spdx"
source="$pkgname-$pkgver.tar.gz::https://github.com/raspberrypi/rpi-firmware/archive/$_commit.tar.gz"
subpackages="$pkgname-common $pkgname-experimental $pkgname-debug $pkgname-cutdown $pkgname-doc"
depends="$pkgname-common=$pkgver-r$pkgrel"

builddir="$srcdir/rpi-firmware-$_commit"

package() {
	local fw; for fw in bootcode.bin fixup.dat fixup4.dat start.elf start4.elf; do
		install -D "$builddir"/$fw \
			"$pkgdir"/boot/$fw
	done
	install -Dm 644 "$builddir"/LICENCE.broadcom \
		"$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

common() {
	pkgdesc="Common files used by Raspberry Pi bootloaders"
	depends=
	amove boot/bootcode.bin
}

experimental() {
	pkgdesc="Experimental firmware with additional codecs"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_x.elf start4x.elf fixup_x.dat fixup4x.dat; do
		install -D "$builddir"/$fw \
				"$subpkgdir"/boot/$fw
	done
}

debug() {
	pkgdesc="Debug firmware"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_db.elf start4db.elf fixup_db.dat fixup4db.dat; do
		install -D "$builddir"/$fw \
			"$subpkgdir"/boot/$fw
	done
}

cutdown() {
	pkgdesc="Cut-down firmware for lower memory settings"
	depends="$pkgname-common=$pkgver-r$pkgrel"
	local fw; for fw in start_cd.elf start4cd.elf fixup_cd.dat fixup4cd.dat; do
		install -D "$builddir"/$fw \
			"$subpkgdir"/boot/$fw
	done
}

sha512sums="
3fda22f2ee4352accbcef86e71b0bee1199c52e42ce5eab3ec6a67d545380e0b523675b968580235e56985fefa4a368dd62b20527f44dac6f35b226cd674d633  raspberrypi-bootloader-1.20230317.tar.gz
"
