# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=cracklib
pkgver=2.9.10
pkgrel=0
pkgdesc="A library used to enforce strong passwords"
url="https://github.com/cracklib/cracklib"
arch="all"
license="LGPL-2.1-or-later"
triggers="$pkgname.trigger=/usr/share/cracklib"
makedepends="zlib-dev"
subpackages="$pkgname-doc $pkgname-dev $pkgname-words::noarch"
source="https://github.com/cracklib/cracklib/releases/download/v$pkgver/cracklib-$pkgver.tar.bz2
	https://github.com/cracklib/cracklib/releases/download/v$pkgver/cracklib-words-$pkgver.gz
	"

# secfixes:
#   2.9.7-r0:
#     - CVE-2016-6318

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--without-python \
		--disable-nls \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	for x in "$pkgdir"/usr/share/cracklib/*; do
		gzip -9 -c "$x" > "$x".gz
		rm "$x"
	done

	install -Dm644 "$srcdir"/cracklib-words-$pkgver.gz "$pkgdir"/usr/share/cracklib/cracklib-words.gz
}

words() {
	pkgdesc="Large list of words for crack/cracklib"
	license="Public-Domain"
	depends=""

	amove usr/share/cracklib/cracklib-words.gz
}

sha512sums="
0c3856833f4c58fc1ebc63cc08b189c55e8d5722ef8cdc1cad5f717cef137a83648657e005e2d0367006b27aafa1bb63e36e7998918937733816635880c85f74  cracklib-2.9.10.tar.bz2
87166906163bab4b79fae253c409bf75bfdd97befc1960b2aaa41b3f3e44a3362dc80d983b2783beb4a99fb1ce0e52316579cc4ba854a0a94321a804ed408a74  cracklib-words-2.9.10.gz
"
