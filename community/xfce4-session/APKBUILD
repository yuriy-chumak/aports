# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=xfce4-session
pkgver=4.18.1
pkgrel=1
pkgdesc="A session manager for Xfce"
url="https://xfce.org/"
arch="all"
license="GPL-2.0-or-later"
subpackages="$pkgname-doc $pkgname-lang"
depends="hicolor-icon-theme iceauth dbus-x11 procps"
makedepends="libxfce4ui-dev xfconf-dev libice-dev xfce4-panel-dev libwnck3-dev dbus-glib-dev intltool"
source="https://archive.xfce.org/src/xfce/xfce4-session/${pkgver%.*}/xfce4-session-$pkgver.tar.bz2
	$pkgname-glib2.76.patch::https://gitlab.xfce.org/xfce/xfce4-session/-/commit/321ca64377eb8ddf5572f260e78f119c9e87dd8b.patch
	busybox-shutdown.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libexecdir=/usr/lib/xfce4 \
		--localstatedir=/var \
		--disable-static \
		--enable-legacy-sm
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
6e6a55138380b6e7ce98508f4d3a52a596882b2f244a1ebd0d1a4b7dd1814c93e6e27453b500cb9178889790f067e28c7c4479b39963511d8e579831a44ef973  xfce4-session-4.18.1.tar.bz2
20a62f4853a8f84246690c9e2f98b6cc54dd6e739314d0157e883a1701e05814e5a96b966446b6e50baec9f54baa64d70b31206ad926cc49896bd663d51fd6e4  xfce4-session-glib2.76.patch
7eab25f534bd4746cf7b8ce8f9245818e47eb0ee73af443c3846dd6d0e3002dced0892142364ce53b688118eafb6d43bbaca8683f85f6103958671328473410c  busybox-shutdown.patch
"
