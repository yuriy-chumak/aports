# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtshadertools
pkgver=6.4.3
pkgrel=0
pkgdesc="Experimental module providing APIs and a host tool to host tool to perform graphics and compute shader conditioning"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	vulkan-headers
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtshadertools-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtshadertools-everywhere-src-${pkgver/_/-}.tar.xz
	"

build() {
	export CFLAGS="$CFLAGS -flto=auto"
	export CXXFLAGS="$CXXFLAGS -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
11d6111c4b645d2d31b19c6cf788726546f484bd9c6a3940bdb2ce82f76eecf56a47655065fb87bf488a12968ea26e48e4c0c8b3ff9d702339bd497bef31630e  qtshadertools-everywhere-src-6.4.3.tar.xz
"
