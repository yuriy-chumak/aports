# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qthttpserver
pkgver=6.4.3
pkgrel=0
pkgdesc="Qt6 HTTP Server"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	qt6-qtwebsockets-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qthttpserver-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qthttpserver-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -flto=auto"
	export CXXFLAGS="$CXXFLAGS -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
025796eb88b1875bbf23f3e97e54831ecf101c0db3622ffbfdde6948f0cef06df83f91a591ef774c27ec17dd9426f7510f80ffd1c9f50cfb351bfa51e14190c0  qthttpserver-everywhere-src-6.4.3.tar.xz
"
