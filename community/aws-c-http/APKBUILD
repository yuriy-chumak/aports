# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=aws-c-http
pkgver=0.7.6
pkgrel=0
pkgdesc="AWS C99 implementation of the HTTP/1.1 and HTTP/2 specifications"
url="https://github.com/awslabs/aws-c-http"
# s390x: aws-c-common
# arm*, ppc64le: aws-c-io
arch="all !armhf !armv7 !ppc64le !s390x"
license="Apache-2.0"
makedepends="
	aws-c-cal-dev
	aws-c-common-dev
	aws-c-compression-dev
	aws-c-io-dev
	cmake
	s2n-tls-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-c-http/archive/refs/tags/v$pkgver.tar.gz"
options="net" # needed for tests to get connections

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	# just test binaries
	# shellcheck disable=2115
	rm -rf "$pkgdir"/usr/bin/
}

dev() {
	default_dev
	amove usr/lib/aws-c-http
}

sha512sums="
3569f68e6bbdf73fd0b9de16e7a7ef01231295e11157e1a3272a7e623ad71a586fe1ea3e0da7535ef3d4b0ac6113dfce39208adf33922c988af1ff3ca9e44bae  aws-c-http-0.7.6.tar.gz
"
