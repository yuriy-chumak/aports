# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtlottie
pkgver=6.4.3
pkgrel=0
pkgdesc="A family of player software for a certain json-based file format for describing 2d vector graphics animations"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtlottie-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtlottie-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1a3e7a462a1a886dfe556816dcb7811f0488ccca64106aebcc8178951f9e3db78dfd977c81b2eaa003b2ceddbd5d42bd1753fda80ad5562cf3e38699eafd1d0f  qtlottie-everywhere-src-6.4.3.tar.xz
"
