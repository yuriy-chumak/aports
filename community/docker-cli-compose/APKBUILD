# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-compose
pkgver=2.17.0
pkgrel=0
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/compose/cli-command"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/docker/compose/archive/v$pkgver.tar.gz"

# secfixes:
#   2.15.1-r0:
#     - CVE-2022-27664
#     - CVE-2022-32149
#   2.12.1-r0:
#     - CVE-2022-39253

_plugin_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/compose-"$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	PKG=github.com/docker/compose/v2
	local ldflags="-X '$PKG/internal.Version=v$pkgver'"
	go build -ldflags="$ldflags" -o docker-compose ./cmd
}

check() {
	# e2e tests are excluded because they depend on live dockerd/kubernetes/ecs
	local pkgs="$(go list ./... | grep -Ev '/(watch|e2e)(/|$)')"
	go test -short $pkgs
	./docker-compose compose version
}

package() {
	install -Dm755 docker-compose -t "$pkgdir$_plugin_installdir"/
}

sha512sums="
65eb057ce492ea56e04a889a65a80649b7cec34f8feec7a5bcfc8324fb831848fc1872e55c4ea19fe26e9fe8b125de2d5d41fc8146a27ddbaccdc5477dd89fb9  docker-cli-compose-2.17.0.tar.gz
"
