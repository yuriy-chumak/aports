# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Contributor: Tuan Hoang <tmhoang@linux.ibm.com>
# Maintainer: Tuan Hoang <tmhoang@linux.ibm.com>
pkgname=yq
pkgver=4.32.2
pkgrel=0
pkgdesc="Portable command-line YAML processor written in Go"
url="https://github.com/mikefarah/yq"
arch="all"
license="MIT"
makedepends="go"
checkdepends="tzdata"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikefarah/yq/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o yq

	./yq shell-completion bash > yq.bash
	./yq shell-completion zsh > yq.zsh
	./yq shell-completion fish > yq.fish
}

check() {
	go test ./...

	# Yanked from scripts/acceptance.sh
	for test in acceptance_tests/*.sh; do
		echo "--------------------------------------------------------------"
		echo "$test"
		echo "--------------------------------------------------------------"
		sh "$test"
	done
}

package() {
	install -Dm755 yq "$pkgdir"/usr/bin/yq

	install -Dm644 yq.bash \
		"$pkgdir"/usr/share/bash-completion/completions/yq
	install -Dm644 yq.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_yq
	install -Dm644 yq.fish \
		"$pkgdir"/usr/share/fish/completions/yq.fish
}

sha512sums="
9cc5b7c694f5a985b9d3118c91632806f359d951abd6ccf7d2c162fb4a079f90efa417c7486db019692897175906fc501a434c353db5bb2b4cd21cd547a6434e  yq-4.32.2.tar.gz
"
