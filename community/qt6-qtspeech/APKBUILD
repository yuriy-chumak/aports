# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtspeech
pkgver=6.4.3
pkgrel=0
pkgdesc="Qt6 module to make text to speech and speech recognition easy"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	alsa-lib-dev
	flite-dev
	qt6-qtdeclarative-dev
	qt6-qtmultimedia-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtspeech-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtspeech-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -flto=auto"
	export CXXFLAGS="$CXXFLAGS -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f101c9a27b300f5b7dfb502f342dc565f206a119f667d02ed23ec0103a2d0d8025d15bccb42bacd238853f695523691dc22e1a4b703f0ea13aab0922c922cb1f  qtspeech-everywhere-src-6.4.3.tar.xz
"
