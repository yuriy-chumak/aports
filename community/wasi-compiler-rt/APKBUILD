# Contributor: psykose <alice@ayaya.dev>
# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=wasi-compiler-rt
# match llvm ver
pkgver=16.0.0
_llvmver="${pkgver%%.*}"
# _wasi_sdk_ver=19
_wasi_sdk_ver=4293ded25742b90e7ab88a140a5dda212de436b2
pkgrel=0
pkgdesc="WASI LLVM compiler runtime"
url="https://compiler-rt.llvm.org/"
arch="all"
options="!check" # TODO: check
license="Apache-2.0 WITH LLVM-exception BSD-2-Clause"
makedepends="
	clang
	cmake
	libxml2-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	python3-dev
	samurai
	wasi-libc
	wasi-libcxx
	zlib-dev
	"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/llvm-project-$pkgver.src.tar.xz
	wasi-sdk-$_wasi_sdk_ver.tar.gz::https://github.com/WebAssembly/wasi-sdk/archive/$_wasi_sdk_ver.tar.gz
	"
builddir="$srcdir"/llvm-project-$pkgver.src

prepare() {
	default_prepare

	mv "$srcdir"/wasi-sdk-$_wasi_sdk_ver/wasi-sdk.cmake "$builddir"
	mv "$srcdir"/wasi-sdk-$_wasi_sdk_ver/cmake/Platform cmake
}

build() {
	export CFLAGS="$CFLAGS -fno-exceptions --sysroot=/usr/share/wasi-sysroot"
	cmake -B build -G Ninja -S compiler-rt -Wno-dev \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_MODULE_PATH="$builddir"/cmake \
		-DCMAKE_TOOLCHAIN_FILE="$builddir"/wasi-sdk.cmake \
		-DCMAKE_C_COMPILER_WORKS=ON \
		-DCMAKE_CXX_COMPILER_WORKS=ON \
		-DCOMPILER_RT_BAREMETAL_BUILD=ON \
		-DCOMPILER_RT_INCLUDE_TESTS=OFF \
		-DCOMPILER_RT_HAS_FPIC_FLAG=OFF \
		-DCOMPILER_RT_DEFAULT_TARGET_ONLY=ON \
		-DCOMPILER_RT_OS_DIR=wasi \
		-DWASI_SDK_PREFIX=/usr \
		-DCMAKE_INSTALL_PREFIX=/usr/lib/llvm$_llvmver/lib/clang/$_llvmver/
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	rm -r "$pkgdir"/usr/lib/llvm$_llvmver/lib/clang/$_llvmver/include
}

sha512sums="
3f040abc1b03205693824aeff2ee6efb0cff25fe04bd5265141c093f294655a1f3fcda73cab9c57cbed7523c8f186a7e2484afce0792c55e90e3fb80450fabb7  llvm-project-16.0.0.src.tar.xz
07ee9160147e3328effd04a2bb5dbd586cc214c4fc3558591dd12ed33f44c589c00eab3e1841ea29ff1d2bb7b743de0284c0b65c56eec92fd8c9fd224f1eab08  wasi-sdk-4293ded25742b90e7ab88a140a5dda212de436b2.tar.gz
"
