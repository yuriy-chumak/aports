# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtquick3d
pkgver=6.4.3
pkgrel=0
pkgdesc="Qt module and API for defining 3D content in Qt Quick"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtdeclarative-dev
	qt6-qtquicktimeline-dev
	qt6-qtshadertools-dev
	vulkan-headers
	"
makedepends="$depends_dev
	assimp-dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtquick3d-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtquick3d-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
511c8585ea8686b1e1cf2e3dd47ac5650ba312d583da7e86dc86aaff6769a5751e3d75ff3fd07664098ba6488f75c9dab68d060b0394ad0182c35c44b5306970  qtquick3d-everywhere-src-6.4.3.tar.xz
"
