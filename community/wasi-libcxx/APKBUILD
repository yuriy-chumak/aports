# Contributor: psykose <alice@ayaya.dev>
# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=wasi-libcxx
# match llvm version
pkgver=16.0.0
_llvmver="${pkgver%%.*}"
#_wasi_sdk_ver=19
_wasi_sdk_ver=4293ded25742b90e7ab88a140a5dda212de436b2
pkgrel=0
pkgdesc="WASI LLVM C++ standard library"
url="https://libcxx.llvm.org/"
arch="all"
license="Apache-2.0 WITH LLVM-exception"
makedepends="
	clang
	cmake
	libxml2-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	python3-dev
	samurai
	wasi-libc
	zlib-dev
	"
source="
	https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/llvm-project-$pkgver.src.tar.xz
	wasi-sdk-$_wasi_sdk_ver.tar.gz::https://github.com/WebAssembly/wasi-sdk/archive/$_wasi_sdk_ver.tar.gz
	"
builddir="$srcdir"/llvm-project-$pkgver.src
# TODO: check, needs to somehow pass wasi sysroot include to lit..
options="!check"

prepare() {
	default_prepare

	mv "$srcdir"/wasi-sdk-$_wasi_sdk_ver/wasi-sdk.cmake "$builddir"
	mv "$srcdir"/wasi-sdk-$_wasi_sdk_ver/cmake/Platform cmake
}

build() {
	export CFLAGS="$CFLAGS -fno-exceptions --sysroot=/usr/share/wasi-sysroot"
	export CXXFLAGS="$CXXFLAGS -fno-exceptions --sysroot=/usr/share/wasi-sysroot"

	cmake -B build -G Ninja -S runtimes -Wno-dev \
		-DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi" \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_MODULE_PATH="$builddir"/cmake \
		-DCMAKE_TOOLCHAIN_FILE="$builddir"/wasi-sdk.cmake \
		-DCMAKE_C_COMPILER_WORKS=ON \
		-DCMAKE_CXX_COMPILER_WORKS=ON \
		-DCMAKE_STAGING_PREFIX=/usr/share/wasi-sysroot \
		-DCXX_SUPPORTS_CXX11=ON \
		-DLIBCXX_ABI_VERSION=2 \
		-DLIBCXX_BUILD_EXTERNAL_THREAD_LIBRARY=OFF \
		-DLIBCXX_CXX_ABI=libcxxabi \
		-DLIBCXX_CXX_ABI_INCLUDE_PATHS=libcxxabi/include \
		-DLIBCXX_ENABLE_EXCEPTIONS=OFF \
		-DLIBCXX_ENABLE_EXPERIMENTAL_LIBRARY=OFF \
		-DLIBCXX_ENABLE_FILESYSTEM=OFF \
		-DLIBCXX_ENABLE_SHARED=OFF \
		-DLIBCXX_ENABLE_THREADS=OFF \
		-DLIBCXX_HAS_EXTERNAL_THREAD_API=OFF \
		-DLIBCXX_HAS_MUSL_LIBC=ON \
		-DLIBCXX_HAS_PTHREAD_API=OFF \
		-DLIBCXX_HAS_WIN32_THREAD_API=OFF \
		-DLIBCXX_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLIBCXX_LIBDIR_SUFFIX=/wasm32-wasi \
		-DLIBCXXABI_BUILD_EXTERNAL_THREAD_LIBRARY=OFF \
		-DLIBCXXABI_ENABLE_EXCEPTIONS=OFF \
		-DLIBCXXABI_ENABLE_PIC=OFF \
		-DLIBCXXABI_ENABLE_SHARED=OFF \
		-DLIBCXXABI_ENABLE_THREADS=OFF \
		-DLIBCXXABI_HAS_EXTERNAL_THREAD_API=OFF \
		-DLIBCXXABI_HAS_PTHREAD_API=OFF \
		-DLIBCXXABI_HAS_WIN32_THREAD_API=OFF \
		-DLIBCXXABI_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLIBCXXABI_LIBCXX_INCLUDES="$builddir"/build-libcxx/include/c++/v1 \
		-DLIBCXXABI_LIBCXX_PATH=libcxx \
		-DLIBCXXABI_LIBDIR_SUFFIX=/wasm32-wasi \
		-DLIBCXXABI_SILENT_TERMINATE:BOOL=ON \
		-DUNIX=ON \
		-DWASI_SDK_PREFIX=/usr
	cmake --build build

	cmake -B build-threads -G Ninja -S runtimes -Wno-dev \
		-DLLVM_ENABLE_RUNTIMES="libcxx;libcxxabi" \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_MODULE_PATH="$builddir"/cmake \
		-DCMAKE_TOOLCHAIN_FILE="$builddir"/wasi-sdk.cmake \
		-DCMAKE_C_COMPILER_WORKS=ON \
		-DCMAKE_CXX_COMPILER_WORKS=ON \
		-DCMAKE_C_FLAGS="$CFLAGS -pthread" \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -pthread" \
		-DCMAKE_STAGING_PREFIX=/usr/share/wasi-sysroot \
		-DCXX_SUPPORTS_CXX11=ON \
		-DLIBCXX_ABI_VERSION=2 \
		-DLIBCXX_BUILD_EXTERNAL_THREAD_LIBRARY=OFF \
		-DLIBCXX_CXX_ABI=libcxxabi \
		-DLIBCXX_CXX_ABI_INCLUDE_PATHS=libcxxabi/include \
		-DLIBCXX_ENABLE_EXCEPTIONS=OFF \
		-DLIBCXX_ENABLE_EXPERIMENTAL_LIBRARY=OFF \
		-DLIBCXX_ENABLE_FILESYSTEM=OFF \
		-DLIBCXX_ENABLE_SHARED=OFF \
		-DLIBCXX_ENABLE_THREADS=ON \
		-DLIBCXX_HAS_EXTERNAL_THREAD_API=OFF \
		-DLIBCXX_HAS_MUSL_LIBC=ON \
		-DLIBCXX_HAS_PTHREAD_API=ON \
		-DLIBCXX_HAS_WIN32_THREAD_API=OFF \
		-DLIBCXX_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLIBCXX_LIBDIR_SUFFIX=/wasm32-wasi-threads \
		-DLIBCXXABI_BUILD_EXTERNAL_THREAD_LIBRARY=OFF \
		-DLIBCXXABI_ENABLE_EXCEPTIONS=OFF \
		-DLIBCXXABI_ENABLE_PIC=OFF \
		-DLIBCXXABI_ENABLE_SHARED=OFF \
		-DLIBCXXABI_ENABLE_THREADS=ON \
		-DLIBCXXABI_HAS_EXTERNAL_THREAD_API=OFF \
		-DLIBCXXABI_HAS_PTHREAD_API=ON \
		-DLIBCXXABI_HAS_WIN32_THREAD_API=OFF \
		-DLIBCXXABI_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLIBCXXABI_LIBCXX_INCLUDES="$builddir"/build-libcxx/include/c++/v1 \
		-DLIBCXXABI_LIBCXX_PATH=libcxx \
		-DLIBCXXABI_LIBDIR_SUFFIX=/wasm32-wasi-threads \
		-DLIBCXXABI_SILENT_TERMINATE:BOOL=ON \
		-DUNIX=ON \
		-DWASI_SDK_PREFIX=/usr
	cmake --build build-threads
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	DESTDIR="$pkgdir" cmake --install build-threads
}

sha512sums="
3f040abc1b03205693824aeff2ee6efb0cff25fe04bd5265141c093f294655a1f3fcda73cab9c57cbed7523c8f186a7e2484afce0792c55e90e3fb80450fabb7  llvm-project-16.0.0.src.tar.xz
07ee9160147e3328effd04a2bb5dbd586cc214c4fc3558591dd12ed33f44c589c00eab3e1841ea29ff1d2bb7b743de0284c0b65c56eec92fd8c9fd224f1eab08  wasi-sdk-4293ded25742b90e7ab88a140a5dda212de436b2.tar.gz
"
